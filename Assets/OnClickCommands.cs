﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickCommands : MonoBehaviour
{
	public void BottomTabFirstRowFirstColumn()
	{
		Debug.Log("Hello World from BottomTabFirstRowFirstColumn()");
	}

	public void BottomTabFirstRowSecondColumn()
	{
		Debug.Log("Hello World from BottomTabFirstRowSecondColumn()");
	}

	public void BottomTabSecondRowFirstColumn()
	{
		Debug.Log("Hello World from BottomTabSecondRowFirstColumn()");
	}

	public void RightTabFirstRow()
	{
		Debug.Log("Hello World from RightTabFirstRow()");
	}
}
